let matchesFile, deliveriesFile;
let xAxis = [];
let yAxis = [];
let won = [];
let team = [];
let chartName = "";
let charSubTitle = "";

let matchFile = function (event) {

    let input = event.target;

    let reader = new FileReader();

    reader.onload = function () {
        let data = reader.result;
        matchesFile = csvJSON(data);
    };
    reader.readAsText(input.files[0]);
};

var deliveryFile = function (event) {

    let input = event.target;

    let reader = new FileReader();

    reader.onload = function () {
        let data = reader.result;
        deliveriesFile = csvJSON(data);
    };
    reader.readAsText(input.files[0]);
};
function csvJSON(data) {

    let lines = data.split("\n");

    let result = [];

    let headers = lines[0].split(",");

    for (let i = 1; i < lines.length - 1; i++) {

        let obj = {};
        let currentline = lines[i].split(",");

        for (let j = 0; j < headers.length; j++) {
            obj[headers[j]] = currentline[j];
        }

        result.push(obj);

    }
    return result;

}


// 1.. NUMBER OF MATCHES PLAYED PER YEAR IN IPL
function numberOfMatches() {
    let seasonYear = [];
    let years = [];
    let matches = [];
    for (let i = 0; i < matchesFile.length; i++) {
        seasonYear[i] = matchesFile[i]['season'];
    }
    years = uniques(seasonYear);
    years.sort();
    let count = 0;
    for (let i = 0; i < years.length; i++) {
        for (let j = 0; j < seasonYear.length; j++) {
            if (years[i] === seasonYear[j]) {
                count++;
            }
        }
        matches.push(count);
        count = 0;
    }
    xAxis = years;
    yAxis = matches;
    chartName = "Number of Matches";
    charSubTitle = "matches player per year in IPL";
    graphPlotting();

}
// to create unqiue array
function uniques(arr) {
    let a = [];
    for (let i = 0, l = arr.length; i < l; i++)
        if (a.indexOf(arr[i]) === -1 && arr[i] !== '')
            a.push(arr[i]);
    return a;
}


// 2.. MATCHES WON BY EACH TEAM
function matchesWon() {
    let seasonYear = [];
    let teamsIPL = [];

    for (let i = 0; i < matchesFile.length; i++) {
        seasonYear[i] = matchesFile[i]['season'];
    }
    let years = uniques(seasonYear);
    years.sort();

    for (let i = 0; i < matchesFile.length; i++) {
        teamsIPL.push(matchesFile[i]['team1']);
    }
    team = uniques(teamsIPL);
    team.sort();

    let count = 0;
    let teamWon = [];

    for (let i = 0; i < team.length; i++) {
        for (let j = 0; j < years.length; j++) {
            for (let k = 0; k < matchesFile.length; k++) {
                if (matchesFile[k]['season'] === years[j] && matchesFile[k]['winner'] === team[i]) {
                    count++;
                }
            }
            teamWon[j] = count;
            count = 0;
        }
        won[i] = teamWon;
        teamWon = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    }

    xAxis = years;

    stackedChartGraph();
}


// 3.. 2016 EXTRA RUNS
function extraRuns() {
    let matchId = [];
    let teamsIPL = [];

    for (let i = 0; i < matchesFile.length; i++) {
        if (matchesFile[i]['season'] == 2016) {
            matchId.push(matchesFile[i]['id']);
        }
    }

    let bowleringTeam = [];

    for (let j = 0; j < matchId.length; j++) {
        for (let i = 0; i < deliveriesFile.length; i++) {
            if (deliveriesFile[i]['match_id'] == matchId[j]) {
                bowleringTeam.push(deliveriesFile[i]['bowling_team']);
            }
        }
    }
    let bowler = uniques(bowleringTeam);
    bowler.sort();

    let score = 0;
    let extraScore = [];
    for (let i = 0; i < bowler.length; i++) {
        for (let j = 0; j < matchId.length; j++) {
            for (let k = 0; k < deliveriesFile.length; k++) {
                if (deliveriesFile[k]['bowling_team'] === bowler[i] && deliveriesFile[k]['match_id'] === matchId[j]) {
                    score = score + parseInt(deliveriesFile[k]['extra_runs']);
                }
            }
        }
        extraScore.push(score);
        score = 0;
    }
    xAxis = bowler;
    yAxis = extraScore;
    chartName = "Extra Runs Conceded per Team";
    charSubTitle = "in 2016";
    graphPlotting();
}


// 4..ECONOMICAL BOWLER
function economicalBowlers() {
    let matchId = [];
    let teamsIPL = [];

    for (let i = 0; i < matchesFile.length; i++) {
        if (matchesFile[i]['season'] == 2015) {
            matchId.push(matchesFile[i]['id']);
        }
    }

    let bowlerName = [];

    for (let j = 0; j < matchId.length; j++) {
        for (let i = 0; i < deliveriesFile.length; i++) {
            if (deliveriesFile[i]['match_id'] == matchId[j]) {
                bowlerName.push(deliveriesFile[i]['bowler']);
            }
        }
    }
    let bowler = uniques(bowlerName);
    bowler.sort();

    let totalrun = [];
    let totalBall = 0;
    let run = 0;
    for (let i = 0; i < bowler.length; i++) {
        for (let j = 0; j < matchId.length; j++) {
            for (let k = 0; k < deliveriesFile.length; k++) {
                if (deliveriesFile[k]['bowler'] === bowler[i] && deliveriesFile[k]['match_id'] === matchId[j]) {
                    run = run + parseInt(deliveriesFile[k]['total_runs']);
                    if (deliveriesFile[i]['wide_runs'] == 0 || deliveriesFile[i]['noball_runs'] == 0) {
                        totalBall++;
                    }
                }
            }
        }
        over = totalBall / 6;
        totalrun.push(run / over);
        run = 0;
        totalBall = 0;
        over = 0;
    }
    let economicalBowler = [];
    for (let i = 0; i < bowler.length; i++) {
        economicalRate = {};
        economicalRate["name"] = bowler[i];
        economicalRate["rate"] = totalrun[i];
        economicalBowler.push(economicalRate);
    }
    economicalBowler.sort(function (obj1, obj2) {
        return obj1.rate - obj2.rate;
    });
    let economicBowler = [];
    let economicRate = [];
    for (let i = 0; i < 10; i++) {
        economicBowler.push(economicalBowler[i]['name']);
        economicRate.push(economicalBowler[i]['rate']);
    }
    console.log(economicBowler);
    console.log(economicRate);
    xAxis = economicBowler;
    yAxis = economicRate;
    chartName = "Economic Bowlers";
    charSubTitle = "in IPL 2015";
    graphPlotting();
}




// PLOTTING GRAPHS
function graphPlotting() {
    let chart = Highcharts.chart('container', {

        title: {
            text: chartName
        },

        subtitle: {
            text: charSubTitle
        },

        xAxis: {
            categories: xAxis
        },

        series: [{
            type: 'column',
            colorByPoint: true,
            data: yAxis,
            showInLegend: false
        }]

    });
}
// STACKED CHART
function stackedChartGraph() {

    Highcharts.chart('container', {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Matches won in all the years of IPL'
        },
        xAxis: {
            categories: xAxis
        },
        yAxis: {
            min: 0,
            title: {
                text: 'matches won by each team in IPL'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
            reversed: true
        },
        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },
        series: [{
            name: team[0],
            data: won[0]
        }, {
            name: team[1],
            data: won[1]
        }, {
            name: team[2],
            data: won[2]
        }, {
            name: team[3],
            data: won[3]
        }, {
            name: team[4],
            data: won[4]
        }, {
            name: team[5],
            data: won[5]
        }, {
            name: team[6],
            data: won[6]
        }, {
            name: team[7],
            data: won[7]
        }, {
            name: team[8],
            data: won[8]
        }, {
            name: team[9],
            data: won[9]
        }, {
            name: team[10],
            data: won[10]
        }, {
            name: team[11],
            data: won[11]
        }, {
            name: team[12],
            data: won[12]
        }, {
            name: team[13],
            data: won[13]
        }]
    });
}